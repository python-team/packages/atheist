#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

__all__ = ['IntField', 'TextField', 'Foreign',
           'Model', 'Schema',
           'IniModel',
           'MissingMandatoryAttr', 'UnknownAttr', 'CastingError', 'MissingSchema']

import sys
import inspect

try:
    from collections import OrderedDict
except ImportError:
    from pyarco.Type import SortedDict as OrderedDict


import pyarco.iniparser
import pyarco.fs

class MissingMandatoryAttr(Exception): pass
class UnknownAttr(Exception): pass
class CastingError(Exception): pass

class MissingSchema(Exception): pass

def getattributes(instance):
    return [item for item in inspect.getmembers(instance, lambda x:not callable(x))
            if not item[0].startswith('__')]

def attr_names(attrs):
    return [x[0] for x in attrs]

class Schema(OrderedDict):

    def __init__(self, *args, **kargs):
        OrderedDict.__init__(self, *args, **kargs)
#        self.__setattr__ = self.proto__setattr__

    def __getattr__(self, key):
        try:
            return self[key]
        except KeyError:
            raise AttributeError

 #   def proto__setattr__(self, key, value):
 #       self[key] = value

    def copy(self):
        return Schema(self)


class Register(dict):
    def __init__(self):
        dict.__init__(self, dict(getattributes(self)))

    def __setattr__(self, key, value):
        self[key] = value


class Field(object):
    cast=None
    def __init__(self, opt=False, auto=False):
        self.opt = opt
        self.auto = auto

        if self.auto:
            self.opt = True

    def to_python(self, value):
        assert self.cast is not None, "You must subclass the Field"
        return self.cast(value)


class TextField(Field):
    cast = str

class IntField(Field):
    cast = int

class Foreign(Field):
    def __init__(self, model, **kargs):
        self.cast = model
        Field.__init__(self, **kargs)


class Model(object):
    def __init__(self, register):
        if not hasattr(self, 'schema') or not isinstance(self.schema, Schema):
            raise MissingSchema()

        schema_mandatory_attr_names = set(self.get_mandatory_attrs().keys())
        schema_all_attr_names = set(self.schema.keys())
        register_attr_names = set(register.keys())

#        print 'schema all      ', self.schema.keys()
#        print 'schema mandatory', self.get_mandatory_attrs().keys()
#        print 'register        ', register.keys()
#        print

        missing_attrs = schema_mandatory_attr_names - register_attr_names
        if missing_attrs:
            raise MissingMandatoryAttr(list(missing_attrs))

        unknown_attrs = register_attr_names - schema_all_attr_names
        if unknown_attrs:
            raise UnknownAttr(register, list(unknown_attrs))

        self.load_register(register)

    def load_register(self, register):
#        print register
        for key,field in self.schema.items():
#            print key, field

            try:
                raw_value = register[key]
            except KeyError:
                if field.opt:
                    continue

            try:
                value = field.to_python(raw_value)
                setattr(self, key, value)

            except (ValueError), e:
                raise CastingError("Can not cast value '{0}' to type '{1}'".format(
                        repr(raw_value), field.cast.__name__))

    @classmethod
    def get_mandatory_attrs(cls):
        return dict(x for x in cls.schema.items() if not x[1].opt)

    @classmethod
    def get_optional_attrs(cls):
        return dict(x for x in cls.schema.items() if x[1].opt)

    @classmethod
    def get_auto_attrs(cls):
        return dict(x for x in cls.schema.items() if x[1].auto)


    def text_render(self):
        retval = ''
        for k,v in self.schema.items():
            retval += "{0}: {1}\n".format(k, getattr(self, k))

        return retval


class IniParser(pyarco.iniparser.IniParser):
    default_section = 'MAIN'


class IniModel(Model):
    FS = pyarco.fs.ActualFileSystem()

    def __init__(self, fname):
        assert isinstance(fname, str)
        ini = IniParser()
        ini.readfp(self.FS.open(fname))
        Model.__init__(self, ini)
