#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

# test utilities

import os
import io


def path_to_items(path):
    if path == os.sep:
        return []
    return path.strip(os.sep).split(os.sep)


class FakeFileSystem:

    class FakeFile(io.BytesIO):
        def close(self):
            self.seek(0)

    def __init__(self, init=None):
        self.dirs = {'/': []}
        self.files = dict()
        if init is not None:
            self.files.update(init)

    def mkdir(self, path):
        if not path:
            raise OSError

        assert path.startswith(os.sep)

        if self.dirs.has_key(path):
            raise OSError

        path = path.strip(os.sep)

        items = path.split('/')
        for i in range(1, len(items)+1):
            key = os.sep + str.join(os.sep, items[:i])
            if not self.dirs.has_key(key):
                self.dirs[key] = []


    def listdir(self, path):
        try:
            # files
            retval = self.dirs[path][:]

            # directories
            path_items = path_to_items(path)
            lon = len(path_items)

            for dname in self.dirs:
                if path == dname:
                    continue

                items = path_to_items(dname)
                if path_items != items[:lon]:
                    continue

                if not items[lon] in retval:
                    retval.append(items[lon])

            return retval

        except KeyError:
            e = OSError()
            e.strerror = 'No such file or directory'
            e.filename = path
            raise e


    def open(self, fname, mode='r', relative=True):
        if 'r' in mode:
            try:
                return self.files[fname]
            except KeyError:
                e = IOError()
                e.strerror="No such file or directory"
                e.filename=fname
                raise e

        if 'w' in mode:
            self.files[fname] = FakeFileSystem.FakeFile()
            return self.files[fname]
