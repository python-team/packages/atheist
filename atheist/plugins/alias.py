# -*- mode:python; coding:utf-8; tab-width:4 -*-

import optparse

import atheist
from atheist.gvar import Log

class AliasValueError(Exception): pass

class Alias(atheist.Plugin):

    @classmethod
    def get_options(cls, config, parser):

        def alias_is_right(value):
            for opt in [x for x in value.split() if x.startswith('--')]:

                if not opt in valid_options:
                    Log.error("[alias] '%s' is not a valid option" % opt)
                    return False

            return True

        if not hasattr(config, 'alias'):
            return []

        valid_options = [x._long_opts[0] for x in parser.option_list]

        retval = []
        for key,value in config.alias.items():
            if alias_is_right(value):
                retval.append(optparse.Option(
                        '--%s' % key, action='store_true',
                        help="User alias for '%s'" % value))

        return retval


    @classmethod
    def config(cls, config, parser):

        def configure_alias(key, value):
            if not hasattr(config, key) or not getattr(config, key):
                return

            Log.info("Applying alias '%s': %s", key, value)
            parser_opts,_ = parser.parse_args(value.split())

            alias_opts = [x for x in value.split() if x.startswith('--')]

            for name in alias_opts:
                attr_name = parser.get_option(name).dest
                value = getattr(parser_opts, attr_name)
                setattr(config, attr_name, value)


        if not hasattr(config, 'alias'):
            return

        for key,value in config.alias.items():
            configure_alias(key, value)
