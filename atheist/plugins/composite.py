# -*- mode: python; coding: utf-8 -*-
""" Composite tasks and conditions """

import types

import atheist
from atheist.const import *


class CompositeCondition(atheist.ConditionDecorator, atheist.Plugin):
    def __init__(self, oper, *conds):
        assert callable(oper)
        self.oper = oper
        atheist.ConditionDecorator.__init__(self, *conds)

    def run(self):
        return self.oper([c.run() for c in self.children])

    @property
    def name(self):
        return self.__class__.__name__

    def basic_info(self):
        return str.join(', ', ["{0}({1})".format(x.name, x.basic_info())
                                                for x in self.children])

class Or(CompositeCondition, atheist.Plugin):
    def __init__(self, *conds):
        CompositeCondition.__init__(self, any, *conds)
