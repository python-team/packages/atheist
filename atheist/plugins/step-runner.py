# -*- mode:python; coding:utf-8 -*-

import sys
import time
import optparse

from pyarco.UI import cls
import atheist
from atheist.gvar import Log, the_config
from atheist.const import *


class StepReporter(atheist.ConsoleReporter):
    def render_task(self, task):
        return self.tree.draw([task], callback=self.build_task)


class StepCaseExecutor(atheist.CaseExecutor):

    def __init__(self, case, observer, reporter):
        atheist.CaseExecutor.__init__(self, case, observer)
        self.reporter = reporter

    def run_tasks(self):
        cls()
        print time.asctime()

        print self.case
        for task in self.case.tasks:
            if self.mng.aborted:
                break

            print task.describe().encode('utf8')
            raw_input("-- Run the task [ENTER]")

            result = atheist.run_task(task, end_callback=self.observer)
            print self.reporter.render_task(task)

            if result in [FAIL,ERROR] and task.check \
                    and not the_config.keep_going:
                task.log.info("FAIL, skipping remaining tasks ('keep-going mode' disabled)")
                break

            raw_input('-- Next task [ENTER]')



class StepRunner(atheist.Runner, atheist.Plugin):

    @classmethod
    def get_options(cls, config, parser):
        return [optparse.Option('--step', action='store_true',
                                help='Execute tests step by step controlled by user')]

    @classmethod
    def config(cls, config, parser):
        if not config.step:
            return

        config.RunnerClass = cls
        config.disable_bar = True


    def run_suite(self):
        reporter = StepReporter('step')

        self.setup()
        results = atheist.Suite()

        for case in self.suite:
            try:
                executor = StepCaseExecutor(case, observer=self.pb.inc, reporter=reporter)
                results.append(executor.run())

                if len(case.tasks) > 1:
                    cls()
                    print "Case report:"
                    print reporter.build_taskcase(case)
                    raw_input('-- Next task [ENTER]')
                    cls()

            except EOFError:
                self.mng.abort()
                break

        cls()
        print "Final report:"
        return results
