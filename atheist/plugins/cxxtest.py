# -*- mode: python; coding: utf-8 -*-

# cxxtest plugin for atheist
#
# Copyright (C) 2010 Cleto Martín, David Villa
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import sys
import os

import atheist


def append_auto_condition(container, condition):
    condition.auto = True
    container += condition

class CxxTest(atheist.CompositeTask, atheist.Plugin):
    acro = 'Cxx'

    class RunningError(Exception): pass

    class MockLibrary(atheist.TestFunc):

        class GoogleMocks:
            def __init__(self, fname):
                self._fname = fname
                self.compiling_flags = '-lgmock -lgtest -lpthread'

            def run(self):
                code_file = open(self._fname, 'rw+')
                code = code_file.readlines()
                code_file.seek(0);
                code_file.writelines(self._add_code(code))
                code_file.close()
                return 0

            def _add_code(self, code):
                main_pattern = 'int main() {\n'
                mark = code.index(main_pattern)

                new_code = ['#include <gmock/gmock.h>\n',
                            'int main(int argc, char **argv) {\n',
                            '::testing::GTEST_FLAG(throw_on_failure) = true;\n',
                            '::testing::InitGoogleMock(&argc, argv);\n',
                            ]

                retval = code[0:mark-1] + new_code + code[mark+1:]
                return retval




        libraries = {'google-mocks':GoogleMocks}

        def __init__(self, mng, library, fname):
            if library not in self.libraries.keys():
                raise RuntimeError("Mock library '%s' not supported" % library)

            self.the_library = self.libraries[library](fname)

            atheist.TestFunc.__init__(self, mng, self.the_library.run)

        def get_compiling_flags(self):
            return self.the_library.compiling_flags

    class Generator(atheist.Subprocess):

        def __init__(self, mng, testfile):


            self.testfile = os.path.abspath(testfile)
            self.outputfile = self._gen_output_filename(self.testfile)
            cmd = 'cxxtestgen --error-printer -o %s %s' % (self.outputfile,
                                                           self.testfile)
            atheist.Subprocess.__init__(self, mng=mng, cmd=cmd, shell=True)
            self.gen += self.outputfile

        def _gen_output_filename(self, fname):
            # /home/user/test.cpp -> ATHEIST_TMP/test_runner
            outfile, extension = os.path.splitext(os.path.basename(fname))
            retval = os.path.join(atheist.const.ATHEIST_TMP,
                                  outfile+'_runner'+extension)
            return retval

        def str_param(self):
            return self.testfile



    class CxxCompiler(atheist.Subprocess):

        def __init__(self, mng, main_file, objs, flags):
            self.main_file = main_file
            self.objs = self._get_objs(objs)
            self.flags = flags
            self.outputfile = os.path.splitext(self.main_file)[0]

            cmd = 'g++ %s %s %s -o %s' % (self.main_file, self.flags,
                                          str.join(' ', self.objs), self.outputfile)
            atheist.Subprocess.__init__(self, mng=mng, cmd=cmd)

            for o in self.objs:
                self.pre += atheist.FileExists(o)
            self.gen += self.outputfile

        def _get_objs(self, objs):
            retval = []
            for dirname,files in objs.items():
                retval.extend([os.path.join(dirname,f) for f in files])
            return retval

        def str_param(self):
            return self.main_file



    def __init__(self, mng, testfile, objs={}, compiling_flags='', runner_env={},
                 mock_library=None, runner_must_fail=False, **kargs):

        self.objs = objs
        self.testfile = testfile
        self.compiling_flags = '-lcxxtest'
        self.mng = mng
        self.mock_library = mock_library
        self.runner_env = runner_env
        self.generated_file = None
        self.runner_must_fail = runner_must_fail

        if compiling_flags:
            self.compiling_flags = compiling_flags + ' ' +self.compiling_flags

        tasks = self._create_tasks()

        atheist.CompositeTask.__init__(self, self.mng, all, *tasks, **kargs)

    def _create_tasks(self):
        generator = self.Generator(self.mng, self.testfile)
        self.generated_file = generator.outputfile
        append_auto_condition(generator.pre, atheist.FileExists(self.testfile))

        if self.mock_library:
            mock = self.MockLibrary(self.mng, self.mock_library, generator.outputfile)
            append_auto_condition(mock.pre, atheist.FileExists(generator.outputfile))

            self.compiling_flags += ' ' + mock.get_compiling_flags()

        compiler = self.CxxCompiler(self.mng, generator.outputfile,
                                    self.objs, self.compiling_flags)
        append_auto_condition(compiler.pre, atheist.FileExists(generator.outputfile))


        runner = atheist.Subprocess(self.mng, compiler.outputfile, env=self.runner_env,
                                    must_fail=self.runner_must_fail)
        append_auto_condition(runner.pre, atheist.FileExists(compiler.outputfile))

        tasks = [generator, compiler, runner]
        if self.mock_library: tasks.insert(1, mock)

        return tasks

    def str_param(self):
        return self.testfile
