# -*- mode:python; coding:utf-8 -*-

# IMAP reader plugin for atheist
#
# Copyright (C) 2010 Oscar Aceña
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import sys, os
import imaplib
import email

import atheist
the_config = atheist.gvar.the_config

def checkResults(items):
    if all(map(lambda x: x == 'OK', items)):
        return atheist.OK
    return atheist.FAIL


class IMAP(atheist.Task, atheist.Plugin):
    acro = 'IMAP'
    allows = ['misconfig']

    def __init__(self, mng, key='imap', **kargs):
        self.key = key
        self.connection = None
        atheist.Task.__init__(self, mng, **kargs)
        self.auto_desc = "IMAP <not configured yet>"

    def configure(self):
        key = self.key

        if not hasattr(the_config, key):
            raise atheist.ConfigureError("Your config requires a section '%s'." % key)

        try:
            self.host = the_config['{0}.host'.format(key)]
            self.port = the_config['{0}.port'.format(key)]
            self.user = the_config['{0}.user'.format(key)]
            self.pasw = the_config['{0}.pasw'.format(key)]
        except AttributeError, e:
            raise atheist.ConfigureError("Your config requires a key '%s.%s'" % (key, e.args[0]))

        try:
            self.port = int(self.port)
        except ValueError, e:
            raise atheist.ConfiggureError("Your config key '%s.port' must be an integer instead of '%s'" %
                                          (key, self.port))

        self.auto_desc = "IMAP {0} -> {1}:{2}".format(self.user, self.host, self.port)


    def run(self):
        try:
            self.connection = imaplib.IMAP4_SSL(self.host, self.port)
            assert self.connection is not None

        except Exception, e:
            self.log.error("IMAP error: %s", e)
            return atheist.FAIL

        try:
            results = (
                self.connection.login(self.user, self.pasw)[0],
                self.connection.select()[0],
                )
        except imaplib.IMAP4.error, e:
            self.log.error("IMAP error: %s", e)
            return atheist.FAIL

        return checkResults(results)

    def filter(self, query, **kargs):
        retval = ImapSearchQuery(self.mng, self, query, **kargs)
        retval.auto_desc = "IMAP filter: {0}".format(query)
        return retval

    def str_param(self):
        return self.key



class ImapSearchQuery(atheist.Task):
    acro = "MSet"

    def __init__(self, mng, reader, query, **kargs):
        self.imap = reader
        self.query = query
        self.messages = []

        atheist.Task.__init__(self, mng, **kargs)

        self.misconfig = self.imap.misconfig


    def configure(self):
        if not self.imap.connection:
            raise atheist.ConfigureError("ImapSearchQuery without IMAP!!")


    def run(self):
        try:
            result, msgIds = self.imap.connection.search(None, self.query)

            results = [result]
            for num in msgIds[0].split():
                result, msg = self.imap.connection.fetch(num, '(RFC822)')
                self.messages.append(email.message_from_string(msg[0][1]))
                results.append(result)

        except Exception, e:
            self.log.error("Error on search: %s" % e)
            return atheist.FAIL

        return checkResults(results)


    def findOnSubjects(self, criteria):
        return ImapSubjectContainsCondition(self, criteria)

    def str_param(self):
        return self.query


class ImapSubjectContainsCondition(atheist.Condition):

    def __init__(self, mailbox, criteria):
        self._mailbox = mailbox
        self.criteria = criteria

        atheist.Condition.__init__(self)

    def run(self):
        for message in self._mailbox.messages:
            if self.criteria in message['Subject']:
                return atheist.OK
        return atheist.FAIL

    def __eq__(self, other):
        return (atheist.Condition.__eq__(self, other) and
                self.criteria == other.criteria and
                self._mailbox == other._mailbox)

    def basic_info(self):
        return "'{0}'".format(self.criteria)
