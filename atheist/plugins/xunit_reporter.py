# -*- mode:python; coding:utf-8 -*-
import sys
import optparse
import socket

import atheist
from atheist.const import *
from atheist.gvar import Log

# from xunit nose plugin
def xmlsafe(s, encoding="utf-8"):
    """Used internally to escape XML."""
    if isinstance(s, unicode):
        s = s.encode(encoding)
    s = str(s)
    for src, rep in [('&', '&amp;', ),
                     ('<', '&lt;', ),
                     ('>', '&gt;', ),
                     ('"', '&quot;', ),
                     ("'", '&quot;', ),
                     ]:
        s = s.replace(src, rep)
    return s


class XUnit_Reporter(atheist.Reporter, atheist.Plugin):
    xunitfile = 'atheist.xml'

    def __init__(self, config, xunitfile=None):
        atheist.Reporter.__init__(self, dst='xunit')
        if xunitfile is None:
            self.xunitfile = XUnit_Reporter.xunitfile
        self.with_hostname = True
        self.with_time = True
        self.data = ''


    def render(self, suite, fd=None):
        if fd is None:
            fd = file(self.xunitfile, 'w')

        head = '<?xml version="1.0" encoding="{encoding}"?>'
        head += '<testsuite name="atheist"'
        if self.with_hostname:
            head += ' hostname="{hostname}"'
        head += ' tests="{total}" errors="{errors}" failures="{failures}"'
        head += ' skip="{skipped}">'

        suite.calculate()
        with fd:
            fd.write(head.format(
                    encoding='UTF-8',
                    hostname=socket.gethostname(),
                    **suite.__dict__))
            fd.write(self.data)
#            for case in suite:
#                fd.write(self._render_case(case))
            fd.write('</testsuite>')


    def add_case(self, case):
        self.data += self._render_case(case)


    def _render_case(self, case):

        def render_reason(task):
            retval = ''
            err_names = {ERROR:'error', FAIL:'failure'}

            if task.result in [ERROR, FAIL]:
                retval += '<{tag} type="{tag}" >{message}</{tag}>'.format(
                    tag = err_names[task.result],
                    message = task.persistent_log.getvalue())

            return retval


        def file_content(fname):
            retval = ''
            try:
                retval = file(fname).read()
            except IOError:
                Log.error("Missing file: %s", fname)

            return xmlsafe(retval) if retval else ''


        def render_outs(task):

            return '''\
<system-out>{stdout}</system-out>\
<system-err>{stderr}</system-err>'''.format(
                stdout = file_content(task.stdout),
                stderr = file_content(task.stderr))

        retval = ''
        for task in case.tasks:
            head = '<testcase classname="{casename}" name="{testname}"'
            if self.with_time:
                head += ' time="{elapsed}" '
            head += ' >'

            xml_case = head.format(
                casename=case.name.lstrip('./'),
                testname=task.name,
                elapsed=case.elapsed)

            xml_case += render_reason(task)
            xml_case += render_outs(task)
            xml_case += '</testcase>'

            retval += xml_case

        return retval


    @classmethod
    def get_options(cls, config, parser):
        retval = []
        retval.append(optparse.Option(
                '--with-xunit', action='store_true', default=False,
                help='Enable plugin Xunit: This plugin provides test results'
                     'in the standard XUnit XML format'))
        retval.append(optparse.Option(
                '--xunit-file', type=str, default=cls.xunitfile, metavar='FILE',
                help='Path to xml file to store the xunit report in. Default'
                     'is %s in the working directory' % cls.xunitfile))
        return retval


    @classmethod
    def config(cls, config, parser):
        if not config.with_xunit:
            return

        config.reporters.append(XUnit_Reporter(config, config.xunit_file))
        config.save_stdout = True
        config.save_stderr = True
