# -*- coding:utf-8; tab-width:4; mode:python -*-
import re

import atheist

class FileContainsRE(atheist.FileContains, atheist.Plugin):
    def run(self):
        try:
            return atheist.FileContains.run(self)
        except re.error, e:
            atheist.Log.warning(e)
            return atheist.const.ERROR


    @classmethod
    def check(cls, expected, content):
        pattern = re.compile(expected)
        return len(pattern.findall(content))
