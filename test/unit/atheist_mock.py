# -*- mode:python; coding:utf-8; tab-width:4 -*-

import atheist

from mock import Mock

class Task(Mock):
    def __init__(self):
        Mock.__init__(self)
        self.pre = atheist.ConditionList()
        self.post = atheist.ConditionList()


class MockManager(atheist.manager.Manager):
    ts = []
    cfg = Mock()
    cfg.timetag = ''
    cfg.screen_width = 80
    cfg.save_stdout = False
    cfg.save_stderr = False
    suite = Mock()
    suite.next = lambda :0
