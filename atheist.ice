// -*- mode: c++; coding: utf-8 -*-

#include <Ice/BuiltinSequences.ice>

module Atheist {

  enum Status {FAIL, OK, NOEXEC, ERR, UNKNOWN, RUNNING, TODO};

  dictionary<string, Status> StatusDict;

  struct TaskStatus {
    Status value;
    StatusDict conditions;
  };

  dictionary<string, TaskStatus> TaskStatusDict;

  struct TaskConfig {
    int    indx;
    string tid;
    bool   ckeck;
    string cwd;
    int    delay;
    string desc;
    bool   detach;
    //    dict   env;
    int    expected;
    bool   mustFail;
    string path;
    int    timeout;
    //    bool   saveStdout;
    //    bool   shell;
    int    signal;
    string stdout;
    Status value;
    long   age;
  };

  dictionary<string, TaskConfig> TaskConfigDict;

  interface TaskCase {
    void run();
    TaskConfigDict getConfig();
    StatusDict getStatus();
  };

  sequence<TaskCase*> TaskCaseSeq;

  interface StatusObserver {
    void updateTasks(StatusDict val);
    void updateTaskCases(StatusDict val);
    void updateManager(Status val);
  };

  interface OutObserver {
    void append(string taskID, string val);
  };

  enum LogLevel {CRITICAL, ERROR, WARNING, INFO, DEBUG};

  struct LogEntry {
    LogLevel level;
    int time;
    string msg;
  };

  sequence<LogEntry> LogEntrySeq;

  interface LogObserver {
    void append(string taskID, LogEntrySeq val);
  };

  interface Manager {
    TaskCaseSeq getTaskCases();
    void runAll();
    void attachStatusOb(StatusObserver* ob);
    void attachOutOb(OutObserver* ob, Ice::StringSeq taskIds);
    void attachLogOb(LogObserver* ob, Ice::StringSeq taskIds);
  };
};
