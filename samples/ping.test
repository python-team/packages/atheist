# -*- mode:python; coding:utf-8 -*-

import signal, re, math, numpy, sys

#ping_obj = re.compile("(\d+) bytes from (.+) \(([\d\.]+)\): icmp_seq=(\d+) ttl=(\d+) time=(?P<time>[\d\.]+)")
ping_obj = re.compile(".+: icmp_seq=(\d+) ttl=(\d+) time=(?P<time>[\d\.]+)")

summary_obj = re.compile("rtt min/avg/max/mdev = ([\d\.]+)/([\d\.]+)/([\d\.]+)/([\d\.]+)")

def mdev(array):
    n = len(array)
    tsum = sum(array)
    tsum2 = sum([x**2 for x in array])
    tsum /= n
    tsum2 /= n
    return math.sqrt(tsum2 - tsum**2)


def print_t(array):
    print ["%.3f" % x for x in array]


class PingTest:
    def __init__(self, task):
        self.life = task.timeout
        self.calculated = []
        self.times = []

        self.lines = file(task.stdout).readlines()

        for f in [self.get_times,
                  self.times_summary]:
            task.post += Callback(f)


    def get_times(self):
        for line in self.lines:
            match = re.search(ping_obj, line)
            if match:
                self.times.append(float(match.group("time")))

        assert len(self.times) == self.life+1

        self.calculated.append(min(self.times))
        self.calculated.append(numpy.average(self.times))
        self.calculated.append(max(self.times))
        self.calculated.append(mdev(self.times))
        return True


    def times_summary(self):
        match = re.search(summary_obj, self.lines[-1])
        assert match
        printed = [float(x) for x in match.groups()]

        print_t(self.times)
        print_t(self.calculated)
        print_t(printed)

        for i in range(3):
            assert abs(self.calculated[i] - printed[i]) < (0.05 * self.calculated[i]), "Precission error"

        return True


def factory(task):
    PingTest(task)
    return True


ok_destinations = [
    'localhost',
    '161.67.27.1',
    'www.uclm.es',
    'www.google.com',
    ]

fail_destinations = [
    ('161.67.27.254', 'Destination Host Unreachable'),
    ('10.0.0.2', '')
    ]

template = Template(timeout=4, signal=signal.SIGINT, save_stdout=True)

ping = args[0] if args[0] else 'ping'

for addr in ok_destinations:
    t = Test('%s %s' % (ping, addr), template=[template])
    t.post += FileContains([\
            "5 packets transmitted",
            "5 received",
            "0% packet loss"])
    t.post += Callback(factory, (t,))


for addr,error in fail_destinations:
    t = Test('%s %s' % (ping, addr), must_fail=True, template=[template])
    t.post += FileContains(["100% packet loss"])
    t.post += FileContains([error])
