.. Atheist documentation master file, created by
   sphinx-quickstart on Tue Aug  4 15:55:15 2009.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Atheist's documentation
=======================

Copyright (C) 2009,2010 David Villa Alises

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU
Free Documentation License".


.. toctree::
   :maxdepth: 2

   intro.rst
   plugins.rst
   examples.rst
   faq.rst
   other.rst

   fdl.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

.. * :ref:`modindex

